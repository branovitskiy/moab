<?php /** @noinspection TypeUnsafeComparisonInspection */

namespace Wikilect\MoabWordstat;


/**
 * Class MoabWordstatClient
 * @package King555\MoabWordstat
 */
class MoabWordstatClient
{

    /**
     * @var int
     */
    private  $attemptsCount=3;

    /**
     * @return int
     */
    public function getAttemptsCount(): int
    {
        return $this->attemptsCount;
    }

    /**
     * @param int $attemptsCount
     */
    public function setAttemptsCount(int $attemptsCount)
    {
        $this->attemptsCount = $attemptsCount;
    }

    /**
     * @var String
     */
    private $apiKey;
    /**
     * @var String
     */
    private $regions;

    /**
     * @var int
     */
    private $delayInterval = 5;

    /**
     * @return String
     */
    public function getRegions(): String
    {
        return $this->regions;
    }

    /**
     * @param String $regions
     */
    public function setRegions(String $regions)
    {
        $this->regions = $regions;
    }

    /**
     * @return int
     */
    public function getDelayInterval(): int
    {
        return $this->delayInterval;
    }

    /**
     * @param int $delayInterval
     */
    public function setDelayInterval(int $delayInterval)
    {
        $this->delayInterval = $delayInterval;
    }

    /**
     * @return int
     */
    public function getMaxTries(): int
    {
        return $this->maxTries;
    }

    /**
     * @param int $maxTries
     */
    public function setMaxTries(int $maxTries)
    {
        $this->maxTries = $maxTries;
    }

    /**
     * @return int
     */
    public function getMaxSize(): int
    {
        return $this->maxSize;
    }

    /**
     * @param int $maxSize
     */
    public function setMaxSize(int $maxSize)
    {
        $this->maxSize = $maxSize;
    }
    /**
     * @var int
     */
    private $maxTries = 10;


    /**
     * @var int
     */
    private $maxSize = 100000;

    /**
     * @var int
     */
    private $sleepOnNetworkErrorDelay = 1;

    /**
     * MoabWordstatClient constructor.
     * @param String $apiKey
     * @param String $regions
     */
    public function __construct(String $apiKey, String $regions)
    {
        $this->apiKey = $apiKey;
        $this->regions = $regions;

    }

    private $_chunkSize = 10000;

    /**
     * @param int $size
     * @return MoabWordstatClient
     */
    public function setChunkSize(int $size): self
    {
        $this->_chunkSize = $size;
        return $this;
    }

    /**
     * @return int
     */
    public function getChunkSize(): int
    {
        return $this->_chunkSize;
    }

    /**
     * Если нужно просто получить ответ, не задумываясь о реализаци, то запрашивать getWordstat()
     * Данный метод позволяет в ручном режиме получать данные и обрабатывать их,
     * сделано это в первую очередь чтобы иметь чуть больше контроля над операциями, но при этом
     * вся логика обработки запросов попрежнему находится здесь: @see checkStatus() @see handlerAnswer()
     *
     * @param array $batch массив фраз
     * @param callable $handler обработчик ответа
     *
     * Example:
     * ```php
     * function(int $id, MoabWordstatStatus $status, array $answer, int $tries): bool {
     *  return true;
     * }
     * ```
     *
     *
     * @throws MoabWordstatException
     */
    public function getWordstatCallable(array $batch, callable $handler): void
    {
        if ($batch === [] || count($batch) >= $this->maxSize) {
            throw new MoabWordstatException('Size is min: 1, max: ' . $this->maxSize);
        }

        $chunks = array_chunk($batch, $this->getChunkSize());
        foreach ($chunks as $portion) {
            $decodedResult = $this->_getWordstat($portion);

            $ids = array_unique(array_merge($decodedResult['added_ids'] ?? [], $decodedResult['exists_ids'] ?? []));
            if (count($ids) === 0) {
                throw new MoabWordstatException('No jobs are added ' . print_r($decodedResult, true));
            }

            foreach ($ids as $id) {

                /**
                 * Опрашиваем состояние
                 */
                $tries = 0;
                while(true) {

                    if (++$tries > $this->maxTries) {
                        throw new MoabWordstatException('Too more tries to get status');
                    }

                    $status = new MoabWordstatStatus($this->checkStatus($id));
                    if ($handler($id, $status, $this->handlerAnswer($status), $tries)) {
                        break;
                    }
                }
            }
        }
    }

    /**
     * @param array $queries
     * @return array
     * @throws MoabWordstatException
     */
    public function getWordstat(Array $queries): array
    {
        if ((count($queries)) >= $this->maxSize) {
            throw new MoabWordstatException("Max size is " . $this->maxSize);
        }
        $portions = array_chunk($queries, 10000);
        $answer = [];
        foreach ($portions as $portion) {
            $decodedResult = $this->_getWordstat($portion);
            $ids = array_unique(array_merge($decodedResult['added_ids'] ?? [], $decodedResult['exists_ids'] ?? []));
            if (count($ids) === 0) {
                throw new MoabWordstatException('No jobs are added ' . print_r($decodedResult, true));
            }
            foreach ($ids as $id) {
                $tries = 0;
                $loaded = false;
                while (!$loaded) {
                    if ($tries++ > $this->maxTries) {
                        throw new MoabWordstatException('Too more tries to get status');
                    }
                    $status = $this->checkStatus($id);
                    switch ($status['status']) {
                        /*0 – новое
                         1 – выполняется
                         2 – завершено
                         3 – приостановлено пользователем
                         4 – приостановлено из-за нехватки баланса
                        */
                        case 0:
                        case 1:
                            sleep($this->delayInterval);
                            break;
                        case 2:
                            // косячный ответ иногда прилетате
                            if ($status['download_zip'] !== NULL) {
                                $csv = MoabWordstatUils::getUnzipGetCsvFile($status['download_zip']);
                                $csvConverted = mb_convert_encoding($csv, 'UTF-8', 'WINDOWS-1251');
                                $result = MoabWordstatUils::parseCsv($csvConverted);

                                $answer = array_merge($result, $answer);
                                $loaded = true;
                            }
                            break;
                        case 3:
                            throw new MoabWordstatException('Someone stopped our job');
                            break;
                        case 4:
                            throw new MoabWordstatException('No money on MOAB account');
                        default:
                            throw new MoabWordstatException('Unknown statis' . $status);

                    }
                }
            }
        }

        return $answer;
    }


    /**
     * @param array $portion
     * @return mixed
     * @throws MoabWordstatException
     */
    private function _getWordstat(Array $portion)
    {
        $task = [
            'phrases_list' => $portion,
            'regions' => $this->regions,
            'syntax' => 1,
            'depth' => 1,
            'db' => 0,
            'group_id' => null,
            'also_suggests' => false,
            'also_check' => false,
            'fix_words_order' => false,
            'type' => 1,
            'minus_words' => [],
            'suggests_types' => [],
            'suggests_depth' => 1
        ];
        $job =
            [
                'task' => $task,
                'api_key' => $this->apiKey,
                'partner_code' => null
            ];

        return $this->apiRequest('https://tools.moab.pro/api/Parse/AddTasks', $job,$this->attemptsCount);

    }


    /**
     * @param $url
     * @param $data
     * @param $maxAttempts;
     * @return mixed
     * @throws MoabWordstatException
     */
    private  function apiRequest($url, $data,$maxAttempts)
    {

        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/json',
                'content' => json_encode($data),
                'ignore_errors' => true
            )
        );

        $context = stream_context_create($opts);

        $result = file_get_contents($url, false, $context);
        if ($result) {
            $decodedData = json_decode($result, true);
            if ($decodedData === NULL) {
                throw new MoabWordstatException('Can not decode data as json '.$result);

            }
            return $decodedData;
        } else {
            if ($maxAttempts<=0) {
                throw new MoabWordstatException('Can not make request or  get data' . $url . ' ' . $result);
            } else {
                sleep($this->sleepOnNetworkErrorDelay);
                return $this->apiRequest($url,$data,$maxAttempts-1);
            }


        }
        /*

        $client = new \GuzzleHttp\Client(['headers' => [
            'Content-Type' => 'application/json'
        ]
        ]);
        try {
            $result = $client->request('POST', $url, [
                'body' => json_encode($data),
                'http_errors' => true
            ]);
        } catch (\Throwable $e) {
            throw new MoabWordstatException('Request sending error ' . $e->getMessage() . ' ' . $url);
        }
        if (in_array($result->getStatusCode(), [200, 401], true)) {
            $decodedData = json_decode($result->getBody(), true);
            if ($decodedData === NULL) {
                throw new MoabWordstatException('Can not decode data as json');

            }
            return $decodedData;
        } else {
            throw new MoabWordstatException('Bad http code ' . $url . ' ' . $result->getBody());

        }
*/

    }


    /**
     * @param int $id
     * @return array
     * @throws MoabWordstatException
     */
    public function checkStatus(int $id): array
    {
        return (array)$this->apiRequest('https://tools.moab.pro/api/Parse/Check', [
            'id' => $id,
            'api_key' => $this->apiKey,
        ], $this->attemptsCount);
    }

    /**
     * schema:
     * - status
     *      0 – новое
     *      1 – выполняется
     *      2 – завершено
     *      3 – приостановлено пользователем
     *      4 – приостановлено из-за нехватки баланса
     * - progress (Прогресс выполнения задания, от 0 до 100)
     * - download_zip
     * - balance (возможно что только для оплаченных аккаунтов)
     *
     * @see https://github.com/moabtools/API/blob/master/README.md
     * @param MoabWordstatStatus $status
     * @return array
     * @throws MoabWordstatException
     */
    public function handlerAnswer(MoabWordstatStatus $status): array
    {
        /**
         * 0 – новое
         * 1 – выполняется
         * 2 – завершено
         * 3 – приостановлено пользователем
         * 4 – приостановлено из-за нехватки баланса
         */
        if ($status->isSuccess()) {
            // косячный ответ иногда прилетате
            $csv = MoabWordstatUils::getUnzipGetCsvFile($status->getFileZip());
            return MoabWordstatUils::parseCsv(mb_convert_encoding($csv, 'UTF-8', 'WINDOWS-1251'));
        }

        return [];
    }
}

