<?php
namespace Wikilect\MoabWordstat;

/**
 * Class MoabWordstatStatus (DTO)
 * @package Wikilect\MoabWordstat
 */
final class MoabWordstatStatus
{
    /**
     * 0 – новое
     * 1 – выполняется
     * 2 – завершено
     * 3 – приостановлено пользователем
     * 4 – приостановлено из-за нехватки баланса
     * @var int
     */
    private $_status;

    /**
     * Прогресс выполнения задания, от 0 до 100
     * @var int
     */
    private $_progress;

    /**
     * @var string
     */
    private $_download_zip;

    /**
     * @var int
     */
    private $_balance;

    /**
     * MoabWordstatStatus constructor.
     * @param array $status
     */
    public function __construct(array $status)
    {
        $this->_status = (int)($status['status'] ?? 0);
        $this->_progress = (int)($status['progress'] ?? 0);
        $this->_download_zip = $status['download_zip'] ?? null;
        $this->_balance = $status['balance'] ?? null;
    }

    /**
     * @return bool
     */
    public function inProcess(): bool
    {
        return in_array($this->_status, [0,1], true);
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->_status === 2 && $this->_download_zip !== null;
    }

    /**
     * @return bool
     */
    public function isStopped(): bool
    {
        return in_array($this->_status, [3,4], true);
    }

    /**
     * @return bool
     */
    public function inProgress(): bool
    {
        return $this->_status === 1;
    }

    /**
     * @return int
     */
    public function getProgress(): int
    {
        return $this->_progress;
    }

    /**
     * @return string|null
     */
    public function getFileZip(): ?string
    {
        return $this->_download_zip;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'status' => $this->_status,
            'progress' => $this->_progress,
            'download_zip' => $this->_download_zip,
            'balance' => $this->_balance
        ];
    }
}